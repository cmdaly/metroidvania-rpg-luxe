package com.bojjenclon.systems;

import luxe.Component;
import luxe.Entity;
import luxe.Sprite;
import luxe.Vector;

import com.bojjenclon.components.Velocity;

class MovementSystem extends Component
{
	public function new()
	{
		super({ name: "MovementSystem" });
	}

	override function init()
	{
		//called when initialising the component
	}

	override function update(p_dt:Float)
    {
      //called every frame for you

     	var l_entitiesInScene:Map<String, Entity> = entity.scene.entities;

     	for (l_entity in l_entitiesInScene.iterator())
     	{
     		if (l_entity.has("Velocity"))
     		{
     			var l_sprite:Sprite = cast(l_entity);
     			var l_velocity:Velocity = l_entity.get("Velocity");

     			if (l_sprite == null || l_velocity == null)
     			{
     				continue;
     			}
          else if (l_velocity.x == 0 && l_velocity.y == 0)
          {
            continue;
          }

          var l_previousPos:Vector = l_entity.pos.clone();

     			l_sprite.pos.x += l_velocity.x;
     			l_sprite.pos.y += l_velocity.y;

          Global.grid.updateEntity(l_entity, l_previousPos);
     		}
     	}
    }

    override function onreset()
    {
        //called when the scene starts or restarts
    }
}
