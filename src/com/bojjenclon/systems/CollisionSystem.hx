package com.bojjenclon.systems;

import luxe.Component;
import luxe.Entity;
import luxe.Sprite;
import luxe.Vector;
import luxe.Rectangle;

import luxe.collision.data.ShapeCollision;
import luxe.collision.ShapeDrawer;
import luxe.collision.ShapeDrawerLuxe;

import com.bojjenclon.components.*;

class CollisionSystem extends Component
{
  private var m_debugDrawer:ShapeDrawer;

  public function new()
  {
    super({ name: "CollisionSystem" });
  }

	override function init()
	{
		//called when initialising the component
    m_debugDrawer = new ShapeDrawerLuxe();
	}

	override function update(p_dt:Float)
  {
    //called every frame for you

   	var l_entitiesInScene:Map<String, Entity> = entity.scene.entities;

   	for (l_entity in l_entitiesInScene.iterator())
   	{
   		if (l_entity.has("Collider"))
   		{
   			var l_sprite:Sprite = cast(l_entity);
        var l_collider:Collider = l_entity.get("Collider");
        var l_hitDelay:HitDelay = l_entity.get("HitDelay");

   			if (l_sprite == null || l_collider == null)
   			{
   				continue;
   			}

        if (l_collider.isStatic)
        {
          continue;
        }

        l_collider.updatePosition();
        //m_debugDrawer.drawShape(l_collider.m_shape);

        checkCollision(l_entity);
   		}
   	}
  }

  override function onreset()
  {
      //called when the scene starts or restarts
  }

  private function checkCollision(p_entity:Entity):Void
  {
    var l_sprite:Sprite = cast(p_entity);
    var l_collider:Collider = p_entity.get("Collider");
    var l_hitDelay:HitDelay = p_entity.get("HitDelay");

    l_collider.collisionOccurred = false;

    var l_rect:Rectangle = new Rectangle(l_sprite.pos.x - l_sprite.origin.x, l_sprite.pos.y - l_sprite.origin.y, l_sprite.size.x, l_sprite.size.y);
    var l_entities:Array<Entity> = Global.grid.getEntitiesInArea(l_rect);

    for (l_otherEntity in l_entities.iterator())
    {
      if (l_otherEntity != p_entity && l_otherEntity.has("Collider"))
      {
        var l_otherCollider:Collider = l_otherEntity.get("Collider");

        if (l_otherCollider == null)
        {
          continue;
        }
        else if (l_otherCollider.collisionOccurred)
        {
          continue;
        }
        else if (!l_otherCollider.isStatic)
        {
          if (l_collider.collisionOccurred)
          {
            continue;
          }

          if (l_hitDelay != null && l_hitDelay.invulnerable)
          {
            continue;
          }
        }

        if (p_entity.has("AnchorPoint"))
        {
          var l_anchor:AnchorPoint = p_entity.get("AnchorPoint");

          if (l_anchor.parent == l_otherEntity)
          {
            continue;
          }
        }

        if (l_otherEntity.has("AnchorPoint"))
        {
          var l_anchor:AnchorPoint = l_otherEntity.get("AnchorPoint");

          if (l_anchor.parent == p_entity)
          {
            continue;
          }
        }

        var l_collisionData:ShapeCollision = l_collider.collidesWith(l_otherCollider);

        if (l_collisionData != null)
        {
          var l_otherTag = l_otherEntity.get("Tag");

          var l_collisionEvent = {
            caller : p_entity,
            other : l_otherEntity,
            separationAxis : l_collisionData.separation
          };

          var l_eventSucceeded:Bool = false;
          if (l_otherTag != null)
          {
            l_eventSucceeded = p_entity.events.fire("collision." + l_otherTag.tag, l_collisionEvent);
          }

          if (l_otherTag == null || l_eventSucceeded == false)
          {
            p_entity.events.fire("collision.generic", l_collisionEvent);
          }

          if (l_collider.collisionOccurred)
          {
            break;
          }
        }
      }
    }
  }
}
