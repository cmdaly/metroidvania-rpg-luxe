package com.bojjenclon.entities.enemies;

import luxe.Scene;
import luxe.Vector;

import com.bojjenclon.entities.EntityOptions;

class CannibalEntity extends EnemyEntity
{
  public function new(p_options:EntityOptions)
  {
    super({
      name: "Cannibal",
      texture: Luxe.resources.texture('assets/gfx/cannibal.gif'),
      pos: (p_options.pos == null ? new Vector(0, 0) : p_options.pos),
      scene: p_options.scene
    });
  }
}
