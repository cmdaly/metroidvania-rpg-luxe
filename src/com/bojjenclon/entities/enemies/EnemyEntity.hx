package com.bojjenclon.entities.enemies;

import luxe.Entity;
import luxe.Sprite;
import luxe.Scene;
import luxe.Vector;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;

import com.bojjenclon.components.*;
import com.bojjenclon.collision.CollisionEvent;
import com.bojjenclon.entities.EntityCountManager;

class EnemyEntity extends CollideableSprite
{
  private function new(p_options:SpriteOptions)
  {
    super(p_options);

    depth = 20;

    add(new Tag("enemy"));
		add(new Health());
 		add(new Velocity());
  	//add(new SpriteCollider(CollisionMethods.enemyHit));
		add(new SpriteCollider());
  	add(new HitDelay());
		add(new Enemy());

    /*var l_animObject = Luxe.resources.json('assets/gfx/playerAnimations.json');
    var l_anim:SpriteAnimation = add(new SpriteAnimation({ name:'Animation' }));
    l_anim.add_from_json_object(l_animObject.asset.json);

    add(new AnimationHandler());

    l_anim.animation = 'idle';
    l_anim.play();*/

    connectEvents();
  }

  override public function connectEvents():Void
  {
    super.connectEvents();

    connectEvent("collision.enemy", enemyCollision);
    connectEvent("collision.player", playerCollision);
    connectEvent("collision.weapon", weaponCollision);
  }

  override public function disconnectEvents():Void
  {
    super.disconnectEvents();

    disconnectEvent("collision.enemy");
    disconnectEvent("collision.player");
    disconnectEvent("collision.weapon");
  }

  private function enemyCollision(p_event:CollisionEvent):Void
  {
    //trace("enemy collision");
  }

  private function playerCollision(p_event:CollisionEvent):Void
  {
    //trace("player collision");
  }

  private function weaponCollision(p_event:CollisionEvent):Void
  {
    //trace("weapon collision");
    var l_weapon:Weapon = p_event.other.get("Weapon");
    var l_weaponParent:Entity = p_event.other.get("AnchorPoint").parent;

    var l_health:Health = get("Health");
    var l_hitDelay:HitDelay = get("HitDelay");

    l_health.hp -= l_weapon.calculateDamage();
    l_hitDelay.hit();
  }
}
