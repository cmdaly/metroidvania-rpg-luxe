package com.bojjenclon.entities;

import luxe.Entity;
import luxe.Sprite;
import luxe.Scene;
import luxe.Vector;
import luxe.components.sprite.SpriteAnimation;

import com.bojjenclon.components.*;
import com.bojjenclon.collision.CollisionEvent;

class PlayerEntity extends CollideableSprite
{
  public function new(p_options:EntityOptions)
  {
    super({
      name: "Player",
      texture: Luxe.resources.texture('assets/gfx/battlemage.gif'),
      pos: (p_options.pos == null ? Luxe.screen.mid : p_options.pos),
      scene: p_options.scene
    });

    depth = 30;

    add(new Tag("player"));
    add(new Health());
    add(new Equipment());
    add(new Velocity());
    add(new PlayerInput());
    add(new SpriteCollider());
    add(new HitDelay());
    add(new Player());

    var l_animObject = Luxe.resources.json('assets/gfx/playerAnimations.json');
    var l_anim:SpriteAnimation = add(new SpriteAnimation({ name:'Animation' }));
    l_anim.add_from_json_object(l_animObject.asset.json);

    add(new AnimationHandler());

    l_anim.animation = 'idle';
    l_anim.play();

    connectEvents();
  }

  override public function connectEvents():Void
  {
    super.connectEvents();

    connectEvent("collision.enemy", enemyCollision);
  }

  override public function disconnectEvents():Void
  {
    super.disconnectEvents();

    disconnectEvent("collision.enemy");
  }

  private function enemyCollision(p_event:CollisionEvent):Void
  {
    var l_otherEntity:Entity = p_event.other;

		var l_health:Health = get("Health");
		l_health.hp--;

		var l_hitDelay:HitDelay = get("HitDelay");
		l_hitDelay.hit();

		trace("You've been hit! You have " + l_health.hp + " out of " + l_health.maxHP + " HP left.");

		if (l_health.hp <= 0)
		{
			/*Global.grid.removeEntity(p_player);

			p_player.destroy();

			p_player = null;*/
      // remove player
      trace("nigga you dead");
		}
  }
}
