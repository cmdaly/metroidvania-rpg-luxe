package com.bojjenclon.entities.equipment;

import luxe.Entity;
import luxe.Sprite;
import luxe.Scene;
import luxe.Vector;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;

import com.bojjenclon.entities.UniqueSprite;
import com.bojjenclon.components.*;
import com.bojjenclon.collision.CollisionEvent;
import com.bojjenclon.entities.EntityCountManager;

typedef WeaponOptions = {
  var parent:Entity;
  var name:String;
}

class WeaponEntity extends UniqueSprite
{
  private var m_data:Dynamic;

  public function new(p_options:WeaponOptions)
  {
    m_data = Luxe.resources.json("assets/data/weapons/" + p_options.name + ".json").asset.json;

    super({
      name: "Weapon",
      texture: Luxe.resources.texture(m_data.gfx),
    	pos: new Vector(p_options.parent.pos.x + m_data.x, p_options.parent.pos.y + m_data.y),
    	scene: p_options.parent.scene
  	});

  	depth = m_data.depth;
		rotation_z = m_data.angle;

		add(new SpriteCollider());
  	add(new AnchorPoint(p_options.parent, new Vector(m_data.x, m_data.y)));
  	add(new Weapon({ name: "Weapon" }, m_data.damage));
  	add(new WeaponAnimation(1.0));
		add(new Tag("weapon"));
  }
}
