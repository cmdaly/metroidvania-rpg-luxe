package com.bojjenclon.entities;

class EntityCountManager
{
  private static var s_entityCounts:Map<String, Int> = new Map<String, Int>();

  private function new()
  {

  }

  public static function addEntity(p_name:String):Int
  {
    if (s_entityCounts.exists(p_name))
    {
      s_entityCounts[p_name] += 1;
    }
    else
    {
      s_entityCounts[p_name] = 1;
    }

    return s_entityCounts[p_name];
  }

  public static function removeEntity(p_name:String):Int
  {
    if (s_entityCounts.exists(p_name))
    {
      s_entityCounts[p_name] -= 1;
    }
    else
    {
      return 0;
    }

    return s_entityCounts[p_name];
  }

  public static function entityCount(p_name:String):Int
  {
    if (s_entityCounts.exists(p_name))
    {
      return s_entityCounts[p_name];
    }
    else
    {
      return 0;
    }
  }

  public static function makeEntityName(p_name:String):String
  {
    if (!s_entityCounts.exists(p_name))
    {
      s_entityCounts[p_name] = 1;
    }

    return p_name + s_entityCounts[p_name];
  }
}
