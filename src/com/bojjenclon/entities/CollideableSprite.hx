package com.bojjenclon.entities;

import luxe.Sprite;
import luxe.Vector;
import luxe.options.SpriteOptions;

import com.bojjenclon.components.Collider;

import com.bojjenclon.collision.CollisionEvent;

class CollideableSprite extends EventSprite
{
  private var m_collisionDetected:Bool;

  public function new(p_spriteOptions:SpriteOptions)
  {
    super(p_spriteOptions);

    m_collisionDetected = false;
  }

  override public function connectEvents():Void
  {
    super.connectEvents();

    connectEvent("collision.generic", genericCollision);
  }

  override public function disconnectEvents():Void
  {
    super.disconnectEvents();

    disconnectEvent("collision.generic");
  }

  private function genericCollision(p_event:CollisionEvent):Void
  {
    var l_thisCollider:Collider = get("Collider");

    if (!l_thisCollider.isStatic)
    {
      l_thisCollider.collisionOccurred = true;
    }

    var l_previousPos:Vector = pos.clone();

    pos.x += (p_event.separationAxis.x * 1.1);
    pos.y += (p_event.separationAxis.y * 1.1);

    Global.grid.updateEntity(this, l_previousPos);
  }
}
