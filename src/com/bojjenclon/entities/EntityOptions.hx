package com.bojjenclon.entities;

import luxe.Scene;
import luxe.Vector;

typedef EntityOptions = {
  var scene:Scene;
  @:optional var pos:Vector;
}
