package com.bojjenclon.entities.obstacles;

import luxe.Entity;
import luxe.Sprite;
import luxe.Scene;
import luxe.Vector;
import luxe.components.sprite.SpriteAnimation;
import luxe.options.SpriteOptions;

import com.bojjenclon.entities.UniqueSprite;
import com.bojjenclon.components.*;
import com.bojjenclon.collision.CollisionEvent;
import com.bojjenclon.entities.EntityCountManager;

typedef CrateOptions = {
  var scene:Scene;
  @:optional var pos:Vector;
  @:optional var big:Bool;
}

class CrateEntity extends UniqueSprite
{
  public function new(p_options:CrateOptions)
  {
    var l_pos:Vector = (p_options.pos == null ? new Vector(0, 0) : p_options.pos);
    var l_gfx:String = ((p_options.big == null || !p_options.big) ? "assets/gfx/crate.gif" : "assets/gfx/bigCrate.png");

    super({
      name: "Crate",
      texture: Luxe.resources.texture(l_gfx),
      pos: l_pos,
      scene: p_options.scene
    });

    depth = 10;

    //add(new Tag("obstacle"));
		add(new SpriteCollider(true));
  }
}
