package com.bojjenclon.entities;

import luxe.Sprite;
import luxe.options.SpriteOptions;

import com.bojjenclon.collision.CollisionEvent;

class EventSprite extends UniqueSprite
{
  private var m_events:Map<String, String>;

  public function new(p_spriteOptions:SpriteOptions)
  {
    super(p_spriteOptions);

    m_events = new Map<String, String>();
  }

  public function connectEvents()
  {
  }

  public function disconnectEvents()
  {
  }

  private function connectEvent(p_eventName:String, p_callback:CollisionEvent->Void)
  {
    if (!m_events.exists(p_eventName))
    {
      m_events[p_eventName] = events.listen(p_eventName, p_callback);
    }
  }

  private function disconnectEvent(p_eventName:String)
  {
    if (m_events.exists(p_eventName))
    {
      events.unlisten(m_events[p_eventName]);
      m_events.remove(p_eventName);
    }
  }
}
