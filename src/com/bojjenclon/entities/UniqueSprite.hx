package com.bojjenclon.entities;

import luxe.Sprite;
import luxe.options.SpriteOptions;

class UniqueSprite extends Sprite
{
  private var m_baseName:String;

  public function new(p_options:SpriteOptions)
  {
    m_baseName = p_options.name;

    super({
      name: EntityCountManager.makeEntityName(m_baseName),
      texture: p_options.texture,
      pos: p_options.pos,
      scene: p_options.scene
    });

    EntityCountManager.addEntity(m_baseName);
  }

  override public function destroy(?p_fromParent:Bool):Void
  {
    super.destroy(p_fromParent);

    EntityCountManager.removeEntity(m_baseName);
  }
}
