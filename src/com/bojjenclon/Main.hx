package com.bojjenclon;

import luxe.Game;
import luxe.Color;
import luxe.Input;
import luxe.Sprite;
import luxe.Vector;
import luxe.Rectangle;
import luxe.Scene;
import luxe.States;
import luxe.Parcel;
import luxe.ParcelProgress;

import com.bojjenclon.states.*;

class Main extends Game
{
	public static var states:States;

	override function ready()
	{
		var preload = new Parcel({
			"jsons" : [
				{ "id" : "assets/gfx/playerAnimations.json" },
				{ "id" : "assets/data/weapons/longSword.json" }
			],
			"textures" : [
	        { "id" : "assets/gfx/battlemage.gif" },
	        { "id" : "assets/gfx/cannibal.gif" },
	        { "id" : "assets/gfx/crate.gif" },
	        { "id" : "assets/gfx/bigCrate.png" },
	        { "id" : "assets/gfx/longSword.gif" }
	    ]
		});

    //but, we also want a progress bar for the parcel,
    //this is a default one, you can do your own
    new ParcelProgress({
        parcel      : preload,
        background  : new Color(1, 1, 1, 0.85),
        oncomplete  : assetsLoaded
    });

    //go!
    preload.load();
	}

	override function onkeyup(p_event:KeyEvent)
	{
		if (p_event.keycode == Key.escape)
		{
			Luxe.shutdown();
		}
	}

	override function update(dt:Float)
	{
	}

	private function assetsLoaded(_):Void
	{
		states = new States({ name: "StateMachine" });
		states.add(new LevelState());
		states.set("LevelState");
	}
}
