package com.bojjenclon;

import luxe.Sprite;

import com.bojjenclon.collision.WorldGrid;

class Global
{
	public static var player:Sprite = null;
	public static var grid:WorldGrid = null;
}