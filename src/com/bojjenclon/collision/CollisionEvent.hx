package com.bojjenclon.collision;

import luxe.Entity;
import luxe.Vector;

typedef CollisionEvent = {
  var caller:Entity;
  var other:Entity;
  var separationAxis:Vector;
};
