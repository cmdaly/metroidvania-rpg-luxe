package com.bojjenclon.collision;

import luxe.Entity;
import luxe.Sprite;
import luxe.Rectangle;
import luxe.Vector;

import luxe.collision.ShapeDrawer;
import luxe.collision.ShapeDrawerLuxe;

class WorldGrid
{
	public var width(get, null):Int;
	public var height(get, null):Int;

	private var m_worldWidth:Int;
	private var m_worldHeight:Int;

	private var m_gridWidth:Int;
	private var m_gridHeight:Int;

	private var m_grid:Array<GridCell>;

	private var m_debugDrawer:ShapeDrawer;

	public function new(p_worldWidth:Float, p_worldHeight:Float, p_gridWidth:Int = 32, p_gridHeight:Int = 32)
	{
		m_worldWidth = Math.ceil(p_worldWidth / p_gridWidth);
		m_worldHeight = Math.ceil(p_worldHeight / p_gridHeight);

		m_gridWidth = p_gridWidth;
		m_gridHeight = p_gridHeight;

		m_grid = new Array<GridCell>();
		for (x in 0 ... m_worldWidth)
		{
			for (y in 0 ... m_worldHeight)
			{
				var l_index:Int = (x + y * m_worldWidth);

				m_grid[l_index] = new GridCell(this, new Rectangle(x * m_gridWidth, y * m_gridHeight, m_gridWidth, m_gridHeight));
			}
		}

		Global.grid = this;

		m_debugDrawer = new ShapeDrawerLuxe();
	}

	public function getCell(?p_pos:Vector, ?p_entity:Entity):GridCell
	{
		if (p_pos != null)
		{
			var l_gridIndex:Int = coordsToIndex(p_pos);

			return m_grid[l_gridIndex];
		}
		else if (p_entity != null)
		{
			var l_gridCoords:Vector = worldToGridCoords(p_entity.pos);
			var l_gridIndex:Int = coordsToIndex(l_gridCoords);

			return m_grid[l_gridIndex];
		}

		return null;
	}

	public function getEntitiesInArea(p_area:Rectangle):Array<Entity>
	{
		var l_entities:Array<Entity> = new Array<Entity>();

		var l_difference:Vector = new Vector(p_area.x % m_gridWidth, p_area.y % m_gridHeight);

		var l_horizontalParts:Int = Math.ceil((p_area.w + l_difference.x) / m_gridWidth);
		var l_verticalParts:Int = Math.ceil((p_area.h + l_difference.y) / m_gridHeight);

		var l_coords:Vector = new Vector(p_area.x, p_area.y);

		var l_checkedIndices:Array<Int> = new Array<Int>();

		for (i in 0 ... l_horizontalParts)
		{
			l_coords.y = p_area.y;

			for (j in 0 ... l_verticalParts)
			{
				var l_gridCoords:Vector = worldToGridCoords(l_coords);
				var l_gridIndex:Int = coordsToIndex(l_gridCoords);

				if (l_checkedIndices.indexOf(l_gridIndex) > -1)
				{
					continue;
				}

				var l_cell:GridCell = m_grid[l_gridIndex];

				for (l_entity in l_cell.iterator())
				{
					if (l_entities.indexOf(l_entity) > -1)
					{
						continue;
					}

					l_entities.push(l_entity);
				}

				l_checkedIndices.push(l_gridIndex);

				l_coords.y += m_gridHeight;
			}

			l_coords.x += m_gridWidth;
		}

		return l_entities;
	}

	public function addEntity(p_entity:Entity):Void
	{
		var l_sprite:Sprite = cast(p_entity);

		var l_area:Rectangle = new Rectangle(p_entity.pos.x - l_sprite.origin.x, p_entity.pos.y - l_sprite.origin.y, l_sprite.size.x, l_sprite.size.y);
		var l_difference:Vector = new Vector(l_area.x % m_gridWidth, l_area.y % m_gridHeight);

		var l_horizontalParts:Int = Math.ceil((l_area.w + l_difference.x) / m_gridWidth);
		var l_verticalParts:Int = Math.ceil((l_area.h + l_difference.y) / m_gridHeight);

		var l_coords:Vector = new Vector(l_area.x, l_area.y);

		for (i in 0 ... l_horizontalParts)
		{
			l_coords.y = l_area.y;

			for (j in 0 ... l_verticalParts)
			{
				var l_gridCoords:Vector = worldToGridCoords(l_coords);
				var l_gridIndex:Int = coordsToIndex(l_gridCoords);

				var l_cell:GridCell = m_grid[l_gridIndex];
				l_cell.add(p_entity);

				l_coords.y += m_gridHeight;
			}

			l_coords.x += m_gridWidth;
		}
	}

	public function removeEntity(p_entity:Entity):Void
	{
		var l_sprite:Sprite = cast(p_entity);

		var l_area:Rectangle = new Rectangle(p_entity.pos.x - l_sprite.origin.x, p_entity.pos.y - l_sprite.origin.y, l_sprite.size.x, l_sprite.size.y);
		var l_difference:Vector = new Vector(l_area.x % m_gridWidth, l_area.y % m_gridHeight);

		var l_horizontalParts:Int = Math.ceil((l_area.w + l_difference.x) / m_gridWidth);
		var l_verticalParts:Int = Math.ceil((l_area.h + l_difference.y) / m_gridHeight);

		var l_coords:Vector = new Vector(l_area.x, l_area.y);

		for (i in 0 ... l_horizontalParts)
		{
			l_coords.y = l_area.y;

			for (j in 0 ... l_verticalParts)
			{
				var l_gridCoords:Vector = worldToGridCoords(l_coords);
				var l_gridIndex:Int = coordsToIndex(l_gridCoords);

				var l_cell:GridCell = m_grid[l_gridIndex];
				l_cell.remove(p_entity);

				l_coords.y += m_gridHeight;
			}

			l_coords.x += m_gridWidth;
		}
	}

	public function updateEntity(p_entity:Entity, p_previousPos:Vector):Void
	{
		if (p_previousPos.equals(p_entity.pos))
		{
			return;
		}

		var l_sprite:Sprite = cast(p_entity);

		// remove from old cells

		var l_area:Rectangle = new Rectangle(p_previousPos.x - l_sprite.origin.x, p_previousPos.y - l_sprite.origin.y, l_sprite.size.x, l_sprite.size.y);
		var l_difference:Vector = new Vector(l_area.x % m_gridWidth, l_area.y % m_gridHeight);

		var l_horizontalParts:Int = Math.ceil((l_area.w + l_difference.x) / m_gridWidth);
		var l_verticalParts:Int = Math.ceil((l_area.h + l_difference.y) / m_gridHeight);

		var l_coords:Vector = new Vector(l_area.x, l_area.y);

		for (i in 0 ... l_horizontalParts)
		{
			l_coords.y = l_area.y;

			for (j in 0 ... l_verticalParts)
			{
				var l_gridCoords:Vector = worldToGridCoords(l_coords);
				var l_gridIndex:Int = coordsToIndex(l_gridCoords);

				var l_cell:GridCell = m_grid[l_gridIndex];
				l_cell.remove(p_entity);

				l_coords.y += m_gridHeight;
			}

			l_coords.x += m_gridWidth;
		}

		// add to new cells

		l_area = new Rectangle(l_sprite.pos.x - l_sprite.origin.x, l_sprite.pos.y - l_sprite.origin.y, l_sprite.size.x, l_sprite.size.y);
		l_difference = new Vector(l_area.x % m_gridWidth, l_area.y % m_gridHeight);

		l_horizontalParts = Math.ceil((l_area.w + l_difference.x) / m_gridWidth);
		l_verticalParts = Math.ceil((l_area.h + l_difference.y) / m_gridHeight);

		l_coords = new Vector(l_area.x, l_area.y);

		for (i in 0 ... l_horizontalParts)
		{
			l_coords.y = l_area.y;

			for (j in 0 ... l_verticalParts)
			{
				var l_gridCoords:Vector = worldToGridCoords(l_coords);
				var l_gridIndex:Int = coordsToIndex(l_gridCoords);

				var l_cell:GridCell = m_grid[l_gridIndex];
				l_cell.add(p_entity);

				l_coords.y += m_gridHeight;
			}

			l_coords.x += m_gridWidth;
		}
	}

	public function worldToGridCoords(p_coordinates:Vector):Vector
	{
		var l_x:Int = Math.floor(p_coordinates.x / m_gridWidth);
		var l_y:Int = Math.floor(p_coordinates.y / m_gridHeight);

		if (l_x < 0)
		{
			l_x = 0;
		}
		else if (l_x >= m_worldWidth)
		{
			l_x = (m_worldWidth - 1);
		}

		if (l_y < 0)
		{
			l_y = 0;
		}
		else if (l_y >= m_worldHeight)
		{
			l_y = (m_worldHeight - 1);
		}

		var l_gridCoords:Vector = new Vector(l_x, l_y);

		return l_gridCoords;
	}

	public function reset():Void
	{
		for (l_cell in m_grid)
		{
			l_cell.reset();
		}
	}

	public function printGrid()
	{
		for (x in 0 ... m_worldWidth)
		{
			for (y in 0 ... m_worldHeight)
			{
				var l_index:Int = (x + y * m_worldWidth);

				var l_cell:GridCell = m_grid[l_index];

				if (l_cell.size() == 0)
				{
					continue;
				}

				var l_output:String = (x + ", " + y + ": ");

				for (l_entity in l_cell.iterator())
				{
					l_output += (l_entity.name + " | ");
				}

				trace(l_output);
			}
		}
	}

	public function debugDraw():Void
	{
		for (l_cell in m_grid)
		{
			l_cell.debugDraw(m_debugDrawer);
		}
	}

	private function coordsToIndex(p_coordinates:Vector):Int
	{
		return Math.floor(p_coordinates.x + p_coordinates.y * m_worldWidth);
	}

	private function get_width():Int
	{
		return m_worldWidth;
	}

	private function get_height():Int
	{
		return m_worldHeight;
	}
}