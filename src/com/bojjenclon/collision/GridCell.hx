package com.bojjenclon.collision;

import luxe.Entity;
import luxe.Rectangle;

import luxe.collision.ShapeDrawer;
import luxe.collision.ShapeDrawerLuxe;
import luxe.collision.shapes.*;

class GridCell
{
	private var m_parent:WorldGrid;
	private var m_cell:Rectangle;
	private var m_contents:Array<Entity>;

	public function new(p_parent:WorldGrid, p_cell:Rectangle)
	{
		m_parent = p_parent;
		m_cell = p_cell;

		m_contents = new Array<Entity>();
	}

	public function add(p_entity:Entity):Bool
	{
		if (m_contents.indexOf(p_entity) > -1)
		{
			return false;
		}

		m_contents.push(p_entity);

		return true;
	}

	public function remove(p_entity:Entity):Bool
	{
		if (m_contents.indexOf(p_entity) < 0)
		{
			return false;
		}

		m_contents.remove(p_entity);

		return true;
	}

	public function iterator():Iterator<Entity>
	{
		return m_contents.iterator();
	}

	public function size():Int
	{
		return m_contents.length;
	}

	public function reset():Void
	{
		m_contents = new Array<Entity>();
	}

	public function debugDraw(p_drawer:ShapeDrawer):Void
	{
		var l_shape:Shape = Polygon.rectangle(m_cell.x, m_cell.y, m_cell.w, m_cell.h, false);
		p_drawer.drawShape(l_shape);
	}
}