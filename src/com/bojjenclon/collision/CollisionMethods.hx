package com.bojjenclon.collision;

import luxe.Entity;
import luxe.Sprite;
import luxe.Color;

import com.bojjenclon.components.*;

class CollisionMethods
{
	public static function playerHit(p_player:Entity, p_otherEntity:Entity):Bool
	{
		var l_otherName:String = p_otherEntity.name.toLowerCase();

		if (p_otherEntity.has("Enemy"))
		{
			var l_health:Health = p_player.get("Health");
			l_health.hp--;

			var l_hitDelay:HitDelay = p_player.get("HitDelay");
			l_hitDelay.hit();

			trace("You've been hit! You have " + l_health.hp + " out of " + l_health.maxHP + " HP left.");

			if (l_health.hp <= 0)
			{
				Global.grid.removeEntity(p_player);

				p_player.destroy();

				p_player = null;
			}
		}
		/*else
		{
			trace("You ran into " + p_otherEntity.name + "!");
		}*/

		return true;
	}

	public static function enemyHit(p_enemy:Entity, p_otherEntity:Entity):Bool
	{
		var l_otherName:String = p_otherEntity.name.toLowerCase();

		if (p_otherEntity.has("Player") || p_otherEntity.has("Weapon"))
		{
			//trace("enemies can go through the player");

			return false;
		}

		return true;
	}

	public static function weaponHit(p_weapon:Entity, p_otherEntity:Entity):Bool
	{
		var l_otherName:String = p_otherEntity.name.toLowerCase();

		if (!p_otherEntity.has("Enemy"))
		{
			//trace("weapons only effect enemies");

			return false;
		}

		var l_otherHitDelay:HitDelay = p_otherEntity.get("HitDelay");

		if (l_otherHitDelay.invulnerable)
		{
			return false;
		}

		var l_weaponData:Weapon = p_weapon.get("Weapon");

		if (!l_weaponData.entityHit(p_otherEntity))
		{
			return false;
		}

		var l_damage:Int = l_weaponData.calculateDamage();

		var l_health:Health = p_otherEntity.get("Health");
		l_health.hp -= l_damage;

		var l_hitDelay:HitDelay = p_otherEntity.get("HitDelay");
		l_hitDelay.hit();

		trace("You hit " + p_otherEntity.name + " for " + l_damage + " damage! " + l_health.hp + " out of " + l_health.maxHP + " HP remaining.");

		if (l_health.hp <= 0)
		{
			Global.grid.removeEntity(p_otherEntity);

			p_otherEntity.destroy();

			p_otherEntity = null;
		}

		return false;
	}
}
