package com.bojjenclon.components;

import luxe.Component;

class Velocity extends Component
{
	public var x:Float;
	public var y:Float;

	public function new()
	{
		super({ name: "Velocity" });
	}

	override function init()
	{
		x = 0;
		y = 0;
  }

  override function onreset()
  {
  	x = 0;
  	y = 0;
  }
}
