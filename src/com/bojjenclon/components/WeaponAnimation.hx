package com.bojjenclon.components;

import luxe.Component;
import luxe.Entity;

class WeaponAnimation extends Component
{
	private var m_elapsed:Float;
	private var m_totalTime:Float;

	public function new(p_animTime:Float)
	{
		super({ name: "WeaponAnimation" });

		m_totalTime = p_animTime;
	}

	override function init()
	{
		m_elapsed = 0;
	}

	override function update(p_dt:Float)
	{
		if (m_elapsed < m_totalTime)
		{
			m_elapsed += p_dt;
		}
		else
		{
			var l_anchor:AnchorPoint = get("AnchorPoint");
			var l_parent:Entity = l_anchor.parent;
			var l_equipment:Equipment = l_parent.get("Equipment");

			l_equipment.weapon = null;

			entity.destroy();
		}
	}

	public function animationFinished():Bool
	{
		return (m_elapsed >= m_totalTime);
	}
}