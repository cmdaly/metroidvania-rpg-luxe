package com.bojjenclon.components;

import luxe.Component;
import luxe.Entity;
import luxe.Sprite;

import luxe.utils.Maths;

class Weapon extends Component
{
	private var m_damage:Int;
	//private var m_element:Elemental;
	private var m_entitiesHit:Array<Entity>; // ensures a weapon can only hit an entity one time per "swing"

	public function new(?p_options:Dynamic, p_damage:Int = 1)
	{
		super(p_options);

		m_damage = p_damage;
		m_entitiesHit = new Array<Entity>();
	}

	public function calculateDamage(p_armor:Int = 0):Int
	{
		return Std.int(Maths.clamp_bottom(m_damage - p_armor, 0));
	}

	public function entityHit(p_entity:Entity):Bool
	{
		if (m_entitiesHit.indexOf(p_entity) > -1)
		{
			return false;
		}

		m_entitiesHit.push(p_entity);

		return true;
	}

	public function clearHits():Void
	{
		m_entitiesHit = new Array<Entity>();
	}
}
