package com.bojjenclon.components;

import luxe.Component;
import luxe.Entity;
import luxe.Vector;

import luxe.collision.shapes.Shape;
import luxe.collision.shapes.Polygon;
import luxe.collision.Collision;
import luxe.collision.data.ShapeCollision;

class Collider extends Component
{
	public var collisionOccurred:Bool;
	public var isStatic:Bool;

	private var m_shape:Shape;

	public function new(?p_shape:Shape, p_static:Bool = false)
	{
		super({ name: "Collider" });

		m_shape = p_shape;

    isStatic = p_static;
	}

	override function init()
	{
		//called when initialising the component
		collisionOccurred = false;
	}

	override function update(p_dt:Float)
  {
      collisionOccurred = false;
  }

  override function onreset()
  {
      //called when the scene starts or restarts
      collisionOccurred = false;
  }

  override function onadded()
  {
    Global.grid.addEntity(entity);
  }

  override function onremoved()
  {
    Global.grid.removeEntity(entity);
  }

  public function updatePosition():Void
  {
    if (m_shape != null)
    {
      m_shape.x = entity.pos.x;
      m_shape.y = entity.pos.y;
    }
  }

  public function collidesWith(p_collider:Collider):ShapeCollision
  {
    if (m_shape == null || p_collider.m_shape == null)
    {
      return null;
    }

  	return Collision.shapeWithShape(m_shape, p_collider.m_shape);
  }
}
