package com.bojjenclon.components;

import luxe.Component;

class Tag extends Component
{
  public var tag:String;

  public function new(p_tag:String)
	{
		super({ name: "Tag" });

    tag = p_tag;
	}
}
