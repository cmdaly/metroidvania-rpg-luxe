package com.bojjenclon.components;

import luxe.Component;

class Health extends Component
{
	public var hp(default, set):Int;
	public var maxHP:Int;

	public function new()
	{
		super({ name: "Health" });
	}

	override function init()
	{
		//called when initialising the component
		hp = maxHP = 10;
	}

	/*override function update(p_dt:Float)
  {
      //called every frame for you
  }

  override function onreset()
  {
      //called when the scene starts or restarts
  }*/

  private function set_hp(p_amount:Int):Int
  {
  	if (p_amount < 0)
  	{
  		hp = 0;
  	}
  	else if (p_amount > maxHP)
  	{
  		hp = maxHP;
  	}
  	else
  	{
  		hp = p_amount;
  	}

  	return hp;
  }
}
