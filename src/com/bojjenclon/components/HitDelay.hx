package com.bojjenclon.components;

import luxe.Component;
import luxe.Color;
import luxe.Sprite;

class HitDelay extends Component
{
	public var invulnerable(get, null):Bool;

	private var m_timer:Float;
	private var m_invulnerableTime:Float;

	private var m_sprite:Sprite;

	public function new(p_frames:Float = 2.5)
	{
		super({ name: "HitDelay" });

		m_invulnerableTime = p_frames;
	}

	override function init():Void
	{
		m_timer = -1;

		m_sprite = cast(entity);
	}

	override function update(p_dt:Float):Void
	{
		if (m_timer > -1)
		{
			m_timer += p_dt;

			if (m_timer >= m_invulnerableTime)
			{
				if (m_sprite != null)
				{
					m_sprite.color = new Color(1, 1, 1);
				}

				m_timer = -1;
			}
		}
	}

	public function hit():Bool
	{
		if (invulnerable)
		{
			return false;
		}

		if (m_sprite != null)
		{
			m_sprite.color = new Color(1, 0, 0);
		}

		m_timer = 0;

		return true;
	}

	private function get_invulnerable():Bool
	{
		return m_timer > -1;
	}
}