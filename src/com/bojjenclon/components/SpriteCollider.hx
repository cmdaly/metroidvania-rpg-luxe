package com.bojjenclon.components;

import luxe.Component;
import luxe.Entity;
import luxe.Sprite;
import luxe.collision.shapes.Shape;
import luxe.collision.shapes.Polygon;
import luxe.collision.Collision;
import luxe.collision.data.ShapeCollision;

class SpriteCollider extends Collider
{
	private var m_sprite:Sprite;

	public function new(p_static:Bool = false)
	{
		super(p_static);
	}

	override function init()
	{
		//called when initialising the component
		super.init();

		m_sprite = cast(entity);
		m_shape = Polygon.rectangle(m_sprite.pos.x, m_sprite.pos.y, m_sprite.size.x, m_sprite.size.y, m_sprite.centered);
	}
}
