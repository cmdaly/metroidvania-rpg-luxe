package com.bojjenclon.components;

import luxe.Component;

import luxe.components.sprite.SpriteAnimation;

class AnimationHandler extends Component
{
	private var m_animation:SpriteAnimation;

	public function new()
	{
		super({ name: "AnimationHandler" });
	}

	override function init()
	{
		m_animation = get("Animation");
	}

	override function update(p_dt:Float)
	{
		if (m_animation.animation == "idle")
		{
			return;
		}
		else if (m_animation.animation == "attack")
		{
			if (!m_animation.playing)
			{
				m_animation.animation = "idle";
				m_animation.play();
			}
		}
	}
}
