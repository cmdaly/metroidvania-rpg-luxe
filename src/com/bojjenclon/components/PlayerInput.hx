package com.bojjenclon.components;

import luxe.Component;
import luxe.Input;
import luxe.Sprite;
import luxe.Vector;
import luxe.components.sprite.SpriteAnimation;
import luxe.utils.Maths;

import com.bojjenclon.entities.equipment.WeaponEntity;

class PlayerInput extends Component
{
	private var m_sprite:Sprite;
	private var m_velocity:Velocity;
	private var m_animation:SpriteAnimation;

	public function new()
	{
		super({ name: "PlayerInput" });
	}

	override function init()
	{
    //called when initialising the component

		m_sprite = cast(entity);
		m_velocity = get("Velocity");
		m_animation = get("Animation");

		Luxe.input.bind_key('left', Key.left);
    Luxe.input.bind_key('left', Key.key_a);

    Luxe.input.bind_key('right', Key.right);
    Luxe.input.bind_key('right', Key.key_d);

    Luxe.input.bind_key('up', Key.up);
    Luxe.input.bind_key('up', Key.key_w);

    Luxe.input.bind_key('down', Key.down);
    Luxe.input.bind_key('down', Key.key_s);

    Luxe.input.bind_key('attack', Key.key_e);
	}

	override function update(p_dt:Float)
	{
	    //called every frame for you

	    if (Luxe.input.mousedown(MouseButton.left))
	    {
	    	var l_difference:Vector = new Vector(Luxe.screen.cursor.pos.x - entity.pos.x, Luxe.screen.cursor.pos.y - entity.pos.y);

	    	if (Math.abs(l_difference.x) > 2)
	    	{
	    		var l_sign:Int = Maths.sign(l_difference.x);

	    		m_velocity.x = (l_sign * 100 * p_dt);
	    	}
	    	else
	    	{
	    		entity.pos.x = Luxe.screen.cursor.pos.x;
	    	}

	    	if (Math.abs(l_difference.y) > 2)
	    	{
	    		var l_sign:Int = Maths.sign(l_difference.y);

	    		m_velocity.y = (l_sign * 100 * p_dt);
	    	}
	    	else
	    	{
	    		entity.pos.y = Luxe.screen.cursor.pos.y;
	    	}
	    }
	    else
	    {
		    if (Luxe.input.inputdown("left"))
		    {
		    	//m_sprite.pos.x -= 1;
		    	m_velocity.x = -100 * p_dt;
		    }
		    else if (Luxe.input.inputdown("right"))
		    {
		    	//m_sprite.pos.x += 1;
		    	m_velocity.x = 100 * p_dt;
		    }
		    else
		    {
		    	m_velocity.x = 0;
		    }

		    if (Luxe.input.inputdown("up"))
		    {
		    	//m_sprite.pos.x -= 1;
		    	m_velocity.y = -100 * p_dt;
		    }
		    else if (Luxe.input.inputdown("down"))
		    {
		    	//m_sprite.pos.x += 1;
		    	m_velocity.y = 100 * p_dt;
		    }
		    else
		    {
		    	m_velocity.y = 0;
		    }

		    if (Luxe.input.inputpressed("attack"))
		    {
		    	if (m_animation.animation != "attack")
		    	{
		    		trace("attacking!");

						new WeaponEntity({
							name: "longSword",
							parent: entity
						});

		    		m_animation.animation = "attack";
		    	}
		    }
		}

		if (entity.pos.x <= 0 && m_velocity.x < 0)
		{
			m_velocity.x = 0;
		}

		if (entity.pos.y <= 0 && m_velocity.y < 0)
		{
			m_velocity.y = 0;
		}

		if (entity.pos.x >= Luxe.renderer.target_size.x && m_velocity.x > 0)
		{
			m_velocity.x = 0;
		}

		if (entity.pos.y >= Luxe.renderer.target_size.y && m_velocity.y > 0)
		{
			m_velocity.y = 0;
		}
	}

	override function onreset()
	{
	    //called when the scene starts or restarts
	}
}
