package com.bojjenclon.components;

import luxe.Component;
import luxe.Entity;
import luxe.Vector;

class AnchorPoint extends Component
{
	public var parent:Entity;
	public var relativePos:Vector;

	public function new(p_parent:Entity, p_relativePosition:Vector)
	{
		super({ name: "AnchorPoint" });

		parent = p_parent;
		relativePos = p_relativePosition;
	}

	override function update(p_dt:Float)
	{
		var l_previousPos:Vector = entity.pos.clone();

		entity.pos.x = (parent.pos.x + relativePos.x);
		entity.pos.y = (parent.pos.y + relativePos.y);

		Global.grid.updateEntity(entity, l_previousPos);
	}
}
