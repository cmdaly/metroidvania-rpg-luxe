package com.bojjenclon.states;

import luxe.Entity;
import luxe.States;
import luxe.Scene;
import luxe.Input;
import luxe.Vector;

import com.bojjenclon.collision.WorldGrid;
import com.bojjenclon.systems.*;
import com.bojjenclon.entities.*;
import com.bojjenclon.entities.enemies.*;
import com.bojjenclon.entities.equipment.*;
import com.bojjenclon.entities.obstacles.*;

class LevelState extends State
{
	private var m_scene:Scene;

	public function new()
	{
		super({ name: "LevelState" });

		//m_scene = new TestScene();
	}

	override function onenter<T>(_value :T)
	{
		Global.grid = new WorldGrid(Luxe.renderer.target_size.x, Luxe.renderer.target_size.y, 50, 50);

		m_scene = new Scene("LevelScene");

		setupEntities();
	}

	override function onkeyup(p_event:KeyEvent)
	{
    if (p_event.keycode == Key.key_r)
		{
			/*Luxe.core.scene.empty();
			Luxe.core.scene.destroy();

			Luxe.core.scene = new TestScene();*/
			reset();
		}
		else if (p_event.keycode == Key.key_g)
		{
			Global.grid.printGrid();
		}
		else if (p_event.keycode == Key.key_l)
		{
			Global.grid.debugDraw();
		}
  }

  public function reset():Void
  {
  	m_scene.empty();

  	Global.grid.reset();

		setupEntities();
  }

	private function setupEntities():Void
	{
		var l_movementSystem:Entity = new Entity({
    		name: "MovementSystem",
    		scene: m_scene
		});
		l_movementSystem.add(new MovementSystem());

		var l_collisionSystem:Entity = new Entity({
			name: "CollisionSystem",
			scene: m_scene
		});
		l_collisionSystem.add(new CollisionSystem());

		Global.player = new PlayerEntity({
			scene: m_scene
		});

		new CannibalEntity({
			scene: m_scene,
			pos: new Vector(50, 50)
		});
		new CrateEntity({
			scene: m_scene,
			pos: new Vector(75, 300)
		});
		new CrateEntity({
			scene: m_scene,
			pos: new Vector(675, 400),
			big: true
		});
	}
}
